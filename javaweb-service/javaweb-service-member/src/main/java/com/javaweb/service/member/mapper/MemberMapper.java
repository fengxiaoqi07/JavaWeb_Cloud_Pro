// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.service.member.entity.Member;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-04
 */
public interface MemberMapper extends BaseMapper<Member> {

}
