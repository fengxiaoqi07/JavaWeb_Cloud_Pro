// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.api.system.entity.User;
import com.javaweb.api.system.entity.UserInfo;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.common.framework.utils.StringUtils;
import com.javaweb.common.security.common.BaseServiceImpl;
import com.javaweb.common.security.utils.SecurityUtils;
import com.javaweb.service.system.config.CommonConfig;
import com.javaweb.service.system.constants.UserConstant;
import com.javaweb.service.system.dto.ResetPwdDto;
import com.javaweb.service.system.dto.UpdatePwdDto;
import com.javaweb.service.system.dto.UpdateUserInfoDto;
import com.javaweb.service.system.entity.Level;
import com.javaweb.service.system.entity.Menu;
import com.javaweb.service.system.entity.Position;
import com.javaweb.api.system.entity.Role;
import com.javaweb.service.system.mapper.*;
import com.javaweb.service.system.query.UserQuery;
import com.javaweb.service.system.service.IMenuService;
import com.javaweb.service.system.service.IUserRoleService;
import com.javaweb.service.system.service.IUserService;
import com.javaweb.service.system.utils.Base64Utils;
import com.javaweb.service.system.utils.CommonUtils;
import com.javaweb.service.system.vo.user.UserInfoVo;
import com.javaweb.service.system.vo.user.UserListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 后台用户管理表 服务实现类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-10-30
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private IMenuService menuService;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private IUserRoleService userRoleService;
    @Autowired
    private LevelMapper levelMapper;
    @Autowired
    private PositionMapper positionMapper;
    @Autowired
    private MenuMapper menuMapper;

    /**
     * 根据用户名获取人员信息
     *
     * @param username 用户名
     * @return
     */
    @Override
    public JsonResult<UserInfo> getInfoByName(String username) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        queryWrapper.eq("mark", 1);
        queryWrapper.last("limit 1");

        User user = userMapper.selectOne(queryWrapper);
        if (StringUtils.isNull(user)) {
            return JsonResult.error("用户名或密码错误");
        }

        UserInfo userInfo = new UserInfo();
        userInfo.setSysUser(user);

        // 临时菜单权限
        Set<String> permissions = new HashSet<String>();
        if (user.getId().equals(1)) {
            // 管理员拥有全部权限
            permissions.add("*:*:*");
        } else {
            // 获取用户权限列表
            List<String> stringList = menuMapper.getPermissionList(user.getId());
            permissions.addAll(stringList);
        }
        userInfo.setPermissions(permissions);

        // 临时角色权限
        Set<String> roles = new HashSet<String>();
        if (user.getId().equals(1)) {
            // 管理员拥有全部权限
            roles.add("admin");
        } else {
            // 获取角色信息
            List<Role> roleList = roleMapper.getRolesByUserId(user.getId());
            if (!roleList.isEmpty()) {
                roleList.forEach(x -> {
                    roles.add(x.getCode());
                });
            }
        }
        userInfo.setRoles(roles);
        return JsonResult.success(userInfo);
    }

    /**
     * 获取用户列表
     *
     * @param userQuery 查询条件
     * @return
     */
    @Override
    public JsonResult getList(UserQuery userQuery) {
        // 查询条件
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        // 真实姓名
        if (!StringUtils.isEmpty(userQuery.getRealname())) {
            queryWrapper.like("realname", userQuery.getRealname());
        }
        // 用户账号
        if (!StringUtils.isEmpty(userQuery.getUsername())) {
            queryWrapper.eq("username", userQuery.getUsername());
        }
        // 性别
        if (StringUtils.isNotNull(userQuery.getGender())) {
            queryWrapper.eq("gender", userQuery.getGender());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("sort");

        // 查询分页数据
        IPage<User> page = new Page<>(userQuery.getPage(), userQuery.getLimit());
        IPage<User> pageData = userMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            UserListVo userListVo = Convert.convert(UserListVo.class, x);
            // 用户头像
            if (!StringUtils.isEmpty(x.getAvatar())) {
                userListVo.setAvatar(CommonUtils.getImageURL(x.getAvatar()));
            }
            // 性别描述
            if (x.getGender() != null && x.getGender() > 0) {
                userListVo.setGenderName(UserConstant.USER_GENDER_LIST.get(x.getGender()));
            }
            // 职级
            if (StringUtils.isNotNull(x.getLevelId())) {
                Level levelInfo = levelMapper.selectById(x.getLevelId());
                if (StringUtils.isNotNull(levelInfo)) {
                    userListVo.setLevelName(levelInfo.getName());
                }
            }
            // 岗位
            if (StringUtils.isNotNull(x.getPositionId())) {
                Position positionInfo = positionMapper.selectById(x.getPositionId());
                if (StringUtils.isNotNull(positionInfo)) {
                    userListVo.setPositionName(positionInfo.getName());
                }
            }
            // 城市数据处理
            if (StringUtils.isNotNull(x.getProvinceCode()) &&
                    StringUtils.isNotNull(x.getCityCode()) &&
                    StringUtils.isNotNull(x.getDistrictCode())) {
                // 初始化数组
                String[] strings = new String[3];
                strings[0] = x.getProvinceCode();
                strings[1] = x.getCityCode();
                strings[2] = x.getDistrictCode();
                userListVo.setCity(strings);
            }
            // 获取角色信息
            List<Role> roleList = roleMapper.getRolesByUserId(x.getId());
            userListVo.setRoles(roleList);
            return userListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 添加或编辑
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(User entity) {
        // 头像处理
        if (!StringUtils.isEmpty(entity.getAvatar()) && entity.getAvatar().contains(CommonConfig.imageURL)) {
            entity.setAvatar(entity.getAvatar().replaceAll(CommonConfig.imageURL, ""));
        }
        // 添加用户设置初始密码
        if (StringUtils.isNull(entity.getId())) {
            entity.setPassword(SecurityUtils.encryptPassword(entity.getPassword()));
        }
        // 省市区处理
        if (entity.getCity().length != 3) {
            return JsonResult.error("请选择省市区");
        }
        // 省份
        entity.setProvinceCode(entity.getCity()[0]);
        // 城市
        entity.setCityCode(entity.getCity()[1]);
        // 省份
        entity.setDistrictCode(entity.getCity()[2]);
        boolean result = this.saveOrUpdate(entity);
        if (!result) {
            return JsonResult.error();
        }
        // 删除已存在的用户角色关系数据
        userRoleService.deleteUserRole(entity.getId());
        // 插入用户角色关系数据
        userRoleService.insertUserRole(entity.getId(), entity.getRoleIds());
        return JsonResult.success();
    }

    /**
     * 根据用户ID删除用户
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public JsonResult deleteById(Integer id) {
        if (StringUtils.isNull(id)) {
            return JsonResult.error("记录ID不能为空");
        }
        // 设置Mark=0
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.set("mark", 0);
        updateWrapper.eq("id", id);
        boolean result = update(updateWrapper);
        if (!result) {
            return JsonResult.error();
        }
        return JsonResult.success("删除成功");
    }

    /**
     * 获取用户信息
     *
     * @return
     */
    @Override
    public JsonResult getUserInfo() {
        Integer userId = SecurityUtils.getLoginUser().getUserId();
        User user = userMapper.selectById(userId);

        // 拷贝属性
        UserInfoVo userInfoVo = new UserInfoVo();
        BeanUtils.copyProperties(user, userInfoVo);
        // 会员头像
        if (StringUtils.isNotNull(user.getAvatar())) {
            userInfoVo.setAvatar(CommonUtils.getImageURL(user.getAvatar()));
        }

        // 获取角色列表
        List<Role> roleList = userRoleMapper.getRolesByUserId(userId);
        userInfoVo.setRoles(roleList);

        // 获取权限菜单
        List<Menu> menuList = menuService.getMenuList(userId);
        userInfoVo.setAuthorities(menuList);

        // 获取节点权限
        List<String> permissionList = menuService.getPermissionList(userId);
        userInfoVo.setPermissionList(permissionList);
        return JsonResult.success(userInfoVo);
    }

    /**
     * 修改密码
     *
     * @param updatePwdDto 参数
     * @return
     */
    @Override
    public JsonResult updatePwd(UpdatePwdDto updatePwdDto) {
        // 用户ID
        Integer userId = SecurityUtils.getLoginUser().getUserId();
        User user = userMapper.selectById(userId);
        if (StringUtils.isNull(user)) {
            return JsonResult.error("用户信息不存在");
        }
        // 验证旧密码是否正确
        if (!SecurityUtils.matchesPassword(updatePwdDto.getOldPassword(), user.getPassword())) {
            return JsonResult.error("旧密码不正确");
        }
        // 设置新密码
        user.setPassword(SecurityUtils.encryptPassword(updatePwdDto.getNewPassword()));
        int reuslt = userMapper.updateById(user);
        if (reuslt == 0) {
            return JsonResult.error("修改密码失败");
        }
        return JsonResult.success("密码修改成功");
    }

    /**
     * 更新个人资料
     *
     * @param updateUserInfoDto 参数
     * @return
     */
    @Override
    public JsonResult updateUserInfo(UpdateUserInfoDto updateUserInfoDto) {
        Integer userId = SecurityUtils.getLoginUser().getUserId();
        // 用户ID验证
        if (StringUtils.isNull(userId)) {
            return JsonResult.error("用户ID不能为空");
        }
        // 实例化用户对象
        User user = new User();
        BeanUtils.copyProperties(updateUserInfoDto, user);
        user.setId(userId);
        // 头像处理
        if (updateUserInfoDto.getAvatar().contains("data:image/")) {
            // 头像Base64转换处理
            String avatar = Base64Utils.saveImg(updateUserInfoDto.getAvatar(), "user");
            user.setAvatar(avatar);
        } else {
            if (!StringUtils.isEmpty(updateUserInfoDto.getAvatar()) && updateUserInfoDto.getAvatar().contains(CommonConfig.imageURL)) {
                user.setAvatar(updateUserInfoDto.getAvatar().replaceAll(CommonConfig.imageURL, ""));
            }
        }
        int result = userMapper.updateById(user);
        if (result == 0) {
            return JsonResult.error("更新失败");
        }
        return JsonResult.success("更新成功");
    }

    /**
     * 重置密码
     *
     * @param resetPwdDto 参数
     * @return
     */
    @Override
    public JsonResult resetPwd(ResetPwdDto resetPwdDto) {
        // 用户ID验证
        if (StringUtils.isNull(resetPwdDto.getId())) {
            return JsonResult.error("用户ID不能为空");
        }
        User user = userMapper.selectById(resetPwdDto.getId());
        if (StringUtils.isNull(user)) {
            return JsonResult.error("用户信息不存在");
        }
        // 设置新密码
        user.setPassword(SecurityUtils.encryptPassword("123456"));
        int reuslt = userMapper.updateById(user);
        if (reuslt == 0) {
            return JsonResult.error("密码重置失败");
        }
        return JsonResult.success("密码重置成功");
    }
}
