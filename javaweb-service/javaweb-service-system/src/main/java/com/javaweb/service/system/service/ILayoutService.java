// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.service;

import com.javaweb.common.framework.common.IBaseService;
import com.javaweb.service.system.entity.Layout;

/**
 * <p>
 * 页面布局管理 服务类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-07
 */
public interface ILayoutService extends IBaseService<Layout> {

}
