// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.framework.common.BaseQuery;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.common.framework.utils.StringUtils;
import com.javaweb.common.security.common.BaseServiceImpl;
import com.javaweb.service.system.entity.ConfigData;
import com.javaweb.service.system.mapper.ConfigDataMapper;
import com.javaweb.service.system.query.ConfigDataQuery;
import com.javaweb.service.system.service.IConfigDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 配置数据表 服务实现类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-06
 */
@Service
public class ConfigDataServiceImpl extends BaseServiceImpl<ConfigDataMapper, ConfigData> implements IConfigDataService {

    @Autowired
    private ConfigDataMapper configDataMapper;

    /**
     * 获取配置列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        ConfigDataQuery configDataQuery = (ConfigDataQuery) query;
        // 查询条件
        QueryWrapper<ConfigData> queryWrapper = new QueryWrapper<>();
        // 配置ID
        if (StringUtils.isNotNull(configDataQuery.getConfigId())) {
            queryWrapper.eq("config_id", configDataQuery.getConfigId());
        }
        // 配置标题
        if (!StringUtils.isEmpty(configDataQuery.getTitle())) {
            queryWrapper.like("title", configDataQuery.getTitle());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("sort");

        // 查询分页数据
        IPage<ConfigData> page = new Page<>(configDataQuery.getPage(), configDataQuery.getLimit());
        IPage<ConfigData> pageData = configDataMapper.selectPage(page, queryWrapper);
        return JsonResult.success(pageData);
    }

    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult setStatus(ConfigData entity) {
        ConfigData config = new ConfigData();
        config.setId(entity.getId());
        config.setStatus(entity.getStatus());
        return super.setStatus(config);
    }

}
