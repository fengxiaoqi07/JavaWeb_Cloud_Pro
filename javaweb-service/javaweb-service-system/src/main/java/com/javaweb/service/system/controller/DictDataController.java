// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;


import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.entity.DictData;
import com.javaweb.service.system.query.DictDataQuery;
import com.javaweb.service.system.service.IDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 字典项管理表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-01
 */
@RestController
@RequestMapping("/dictdata")
public class DictDataController extends BaseController {

    @Autowired
    private IDictDataService dictDataService;

    /**
     * 获取字典项列表
     *
     * @param dictDataQuery 查询条件
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dictionary:index')")
    @GetMapping("/index")
    public JsonResult index(DictDataQuery dictDataQuery) {
        return dictDataService.getList(dictDataQuery);
    }

    /**
     * 添加字典项
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dictionary:add')")
    @PostMapping("/add")
    public JsonResult add(@RequestBody DictData entity) {
        return dictDataService.edit(entity);
    }

    /**
     * 编辑字典项
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dictionary:edit')")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody DictData entity) {
        return dictDataService.edit(entity);
    }

    /**
     * 删除字典项
     *
     * @param dictIds 字典项ID
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dictionary:delete')")
    @DeleteMapping("/delete/{dictIds}")
    public JsonResult delete(@PathVariable("dictIds") Integer[] dictIds) {
        return dictDataService.deleteByIds(dictIds);
    }

}
