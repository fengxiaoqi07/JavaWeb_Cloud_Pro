// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.framework.common.BaseQuery;
import com.javaweb.common.framework.utils.DateUtils;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.common.security.common.BaseServiceImpl;
import com.javaweb.service.system.entity.Example4;
import com.javaweb.service.system.mapper.Example4Mapper;
import com.javaweb.service.system.query.Example4Query;
import com.javaweb.service.system.service.IExample4Service;
import com.javaweb.service.system.vo.example4.Example4InfoVo;
import com.javaweb.service.system.vo.example4.Example4ListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 演示案例表 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2021-05-15
  */
@Service
public class Example4ServiceImpl extends BaseServiceImpl<Example4Mapper, Example4> implements IExample4Service {

    @Autowired
    private Example4Mapper example4Mapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        Example4Query example4Query = (Example4Query) query;
        // 查询条件
        QueryWrapper<Example4> queryWrapper = new QueryWrapper<>();
        // 案例名称
        if (!StringUtils.isEmpty(example4Query.getName())) {
            queryWrapper.like("name", example4Query.getName());
        }
        // 类型：1京东 2淘宝 3拼多多 4唯品会
        if (!StringUtils.isEmpty(example4Query.getType())) {
            queryWrapper.eq("type", example4Query.getType());
        }
        // 状态：1正常 2停用
        if (!StringUtils.isEmpty(example4Query.getStatus())) {
            queryWrapper.eq("status", example4Query.getStatus());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<Example4> page = new Page<>(example4Query.getPage(), example4Query.getLimit());
        IPage<Example4> pageData = example4Mapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            // TODO...
            return x;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        Example4 entity = (Example4) super.getInfo(id);
        // 返回视图Vo
        Example4InfoVo example4InfoVo = new Example4InfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, example4InfoVo);
        return example4InfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(Example4 entity) {
        if (entity.getId() != null && entity.getId() > 0) {
            entity.setUpdateUser(1);
            entity.setUpdateTime(DateUtils.now());
        } else {
            entity.setCreateUser(1);
            entity.setCreateTime(DateUtils.now());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(Example4 entity) {
        entity.setUpdateUser(1);
        entity.setUpdateTime(DateUtils.now());
        entity.setMark(0);
        return super.delete(entity);
    }
}