// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;

import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.entity.Example4;
import com.javaweb.service.system.query.Example4Query;
import com.javaweb.service.system.service.IExample4Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 演示案例表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2021-05-15
 */
@RestController
@RequestMapping("/example4")
public class Example4Controller extends BaseController {

    @Autowired
    private IExample4Service example4Service;

    /**
     * 获取数据列表
     *
     * @param example4Query 查询条件
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:example4:index')")
    @GetMapping("/index")
    public JsonResult index(Example4Query example4Query) {
        return example4Service.getList(example4Query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:example4:add')")
    @PostMapping("/add")
    public JsonResult add(@RequestBody Example4 entity) {
        return example4Service.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param example4Id 记录ID
     * @return
     */
    @GetMapping("/info/{example4Id}")
    public JsonResult info(@PathVariable("example4Id") Integer example4Id) {
        return example4Service.info(example4Id);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:example4:edit')")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Example4 entity) {
        return example4Service.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param example4Ids 记录ID
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:example4:delete')")
    @DeleteMapping("/delete/{example4Ids}")
    public JsonResult delete(@PathVariable("example4Ids") Integer[] example4Ids) {
        return example4Service.deleteByIds(example4Ids);
    }

    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:example4:status')")
    @PutMapping("/setStatus")
    public JsonResult setStatus(@RequestBody Example4 entity) {
        return example4Service.setStatus(entity);
    }
}