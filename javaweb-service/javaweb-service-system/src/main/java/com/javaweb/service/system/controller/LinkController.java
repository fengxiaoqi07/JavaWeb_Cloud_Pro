// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;


import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.entity.Link;
import com.javaweb.service.system.query.LinkQuery;
import com.javaweb.service.system.service.ILinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 友链管理表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-07
 */
@RestController
@RequestMapping("/link")
public class LinkController extends BaseController {

    @Autowired
    private ILinkService linkService;

    /**
     * 获取友链列表
     *
     * @param linkQuery 查询条件
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:link:index')")
    @GetMapping("/index")
    public JsonResult index(LinkQuery linkQuery) {
        return linkService.getList(linkQuery);
    }

    /**
     * 添加友链
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:link:add')")
    @PostMapping("/add")
    public JsonResult add(@RequestBody Link entity) {
        return linkService.edit(entity);
    }

    /**
     * 编辑友链
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:link:edit')")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Link entity) {
        return linkService.edit(entity);
    }

    /**
     * 删除友链
     *
     * @param linkIds 友链ID
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:link:delete')")
    @DeleteMapping("/delete/{linkIds}")
    public JsonResult delete(@PathVariable("linkIds") Integer[] linkIds) {
        return linkService.deleteByIds(linkIds);
    }

    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:link:status')")
    @PutMapping("/status")
    public JsonResult status(@RequestBody Link entity) {
        return linkService.setStatus(entity);
    }

}
