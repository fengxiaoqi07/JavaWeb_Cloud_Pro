// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;


import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.entity.Dept;
import com.javaweb.service.system.query.DeptQuery;
import com.javaweb.service.system.service.IDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-03
 */
@RestController
@RequestMapping("/dept")
public class DeptController extends BaseController {

    @Autowired
    private IDeptService deptService;

    /**
     * 获取部门列表
     *
     * @param deptQuery 查询条件
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dept:index')")
    @GetMapping("/index")
    public JsonResult index(DeptQuery deptQuery) {
        return deptService.getList(deptQuery);
    }

    /**
     * 添加部门
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dept:add')")
    @PostMapping("/add")
    public JsonResult add(@RequestBody Dept entity) {
        return deptService.edit(entity);
    }

    /**
     * 编辑部门
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dept:edit')")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Dept entity) {
        return deptService.edit(entity);
    }

    /**
     * 删除部门
     *
     * @param deptId 部门ID
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dept:delete')")
    @DeleteMapping("/delete/{deptId}")
    public JsonResult delete(@PathVariable("deptId") Integer deptId) {
        return deptService.deleteById(deptId);
    }

    /**
     * 获取部门列表
     *
     * @return
     */
    @GetMapping("/getDeptList")
    public JsonResult getDeptList() {
        return deptService.getDeptList();
    }

}
