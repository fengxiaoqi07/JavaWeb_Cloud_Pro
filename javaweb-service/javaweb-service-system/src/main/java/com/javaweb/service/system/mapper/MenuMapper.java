// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaweb.service.system.entity.Menu;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 系统菜单表 Mapper 接口
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-10-30
 */
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 根据用户ID获取权限列表
     *
     * @param userId 用户ID
     * @param pid    上级ID
     * @return
     */
    List<Menu> getPermissionsListByUserId(@Param("userId") Integer userId, @RequestParam("pid") Integer pid);

    /**
     * 获取所有权限
     *
     * @return
     */
    List<Menu> getPermissionsAll();

    /**
     * 获取用户权限节点列表
     *
     * @param userId 用户ID
     * @return
     */
    List<String> getPermissionList(@Param("userId") Integer userId);

}
