// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.query;

import com.javaweb.common.framework.common.BaseQuery;
import lombok.Data;

/**
 * 字典数据查询条件
 */
@Data
public class DictDataQuery extends BaseQuery {

    /**
     * 字典数据名称
     */
    private String name;

    /**
     * 字典数据编码
     */
    private String code;

    /**
     * 字典ID
     */
    private Integer dictId;

}
