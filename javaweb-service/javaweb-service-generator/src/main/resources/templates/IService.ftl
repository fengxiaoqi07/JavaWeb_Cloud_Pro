// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package ${packageName}.service;

import com.javaweb.common.framework.common.IBaseService;
import ${packageName}.entity.${entityName};

/**
 * <p>
 * ${tableAnnotation} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
public interface I${entityName}Service extends IBaseService<${entityName}> {

}